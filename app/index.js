import Phaser from 'phaser'

const config = {
    type: Phaser.CANVAS,
    width: 960,
    height: 720,
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

const game = new Phaser.Game(config);

const clothes = {
    bottom: ['bottom1', 'bottom2', 'bottom3', 'bottom4', 'bottom5', 'bottom6'],
    top: ['top1', 'top2', 'top3', 'top4', 'top5', 'top6'],
    dress: ['dress1', 'dress2', 'dress3', 'dress4']
}

const state = {
    sprites: {
        background: null,
        princess: null,
        ui: {
            btnDress: null,
            btnTops: null,
            btnBottoms: null,
            arrowUp: null,
            arrowDown: null,
            clothesBackground: null
        },
        clothes: {
            wear: {},
            hanger: {}
        }
    },
    clothes: {
        hanger: {
            shownGroup: 'dress',
            currentItem: 1
        },
        wear: {
            dress: null,
            top: null,
            bottom: null
        }
    },
    audio: {},
    tweens: {}
}

function preload () {
    ASSETS_PATHS.forEach((path) => {
        const arrayPath = path.replace(/\..*/, '').replace(/\//g, '\\').split('\\')
        const assetName = arrayPath.slice(2).join('_')
        if (arrayPath[1] === 'image') {
            this.load.image(assetName, path)
        } else if (arrayPath[1] === 'audio') {
            this.load.audio(assetName, path)
        }
    })


    const background = this.add.graphics({ fillStyle: { color: 0 } })
    const progressBar = this.add.graphics({ fillStyle: { color: 0x222222, alpha: 0.8 } })
    const progressBox = this.add.graphics({ fillStyle: { color: 0x4d4d4d } })

    background.depth = 100
    progressBar.depth = 100
    progressBox.depth = 100

    background.fillRect(0, 0, 960, 720)
    progressBar.fillRect(320, 335, 320, 50)

    this.load.on('progress', (value) => {
        progressBox.fillRect(320, 335, 320 * value, 50)
    });
     
    this.load.on('complete', () => {
        state.audio.main = this.sound.add('hipshop');
        const play = this.add.sprite(480, 360, 'ui_play').setInteractive()
        play.depth = 100
        play.on('pointerdown', (pointer) => {
            background.destroy()
            progressBar.destroy()
            progressBox.destroy()
            play.destroy()
            state.tweens.princess.play()
            state.audio.main.play({
                loop: true,
                volume: 0.1
            })
            const textBackground = this.add.graphics({ fillStyle: { color: 0, alpha: 0.4 } })
            const text = this.add.text(480, 670, 'Наряди Принцессу', { font: "40px Georgia", color: 'white' }).setOrigin(0.5, 0.5);
            textBackground.fillRect(295, 645, 370, 60)
        })
    });
}

function create () {
    state.sprites.background = this.add.image(0, 0, 'background').setOrigin(0, 0)
    state.sprites.princess = this.add.image(0, 514, 'princess')
    state.sprites.ui.btnDress = this.add.sprite(63, 175, 'ui_btnDress').setInteractive()
    state.sprites.ui.btnTops = this.add.sprite(63, 287, 'ui_btnTops').setInteractive()
    state.sprites.ui.btnBottoms = this.add.sprite(63, 399, 'ui_btnBottoms').setInteractive()
    state.sprites.ui.arrowUp = this.add.sprite(890, 215, 'ui_arrowUp').setInteractive()
    state.sprites.ui.arrowDown = this.add.sprite(890, 390, 'ui_arrowDown').setInteractive()
    state.sprites.ui.clothesBackground = this.add.sprite(654, 313, 'ui_clothesBackground')
    state.sprites.ui.resetClothes = this.add.text(45, 680, '🔃', { fontSize: "40px", color: 'white' }).setInteractive()
    state.sprites.ui.toggleAudio = this.add.text(5, 680, '🔊', { fontSize: "40px", color: 'white' }).setInteractive()

    for (let group in clothes) {
        clothes[group].forEach((item) => {
            state.sprites.clothes.wear[item] = this.add.sprite(0, 514, `clothes_wear_${item}`)
            state.sprites.clothes.hanger[item] = this.add.sprite(658, 287, `clothes_hanger_${item}`).setInteractive()
            state.sprites.clothes.hanger[item].on('pointerdown', (pointer) => {
                wearCloth(group, item)
            })

            state.sprites.clothes.wear[item].visible = false
            state.sprites.clothes.hanger[item].visible = false
        })
    }

    state.sprites.ui.btnDress.on('pointerdown', (pointer) => {
        changeHangerGroup('dress')
    })

    state.sprites.ui.btnTops.on('pointerdown', (pointer) => {
        changeHangerGroup('top')
    })

    state.sprites.ui.btnBottoms.on('pointerdown', (pointer) => {
        changeHangerGroup('bottom')
    })

    state.sprites.ui.arrowDown.on('pointerdown', (pointer) => {
        arrowDown()
    })

    state.sprites.ui.arrowUp.on('pointerdown', (pointer) => {
        arrowUp()
    })

    state.sprites.ui.resetClothes.on('pointerdown', (pointer) => {
        resetClothes()
    })

    state.sprites.ui.toggleAudio.on('pointerdown', (pointer) => {
        if (state.audio.main.isPaused === true) {
            state.audio.main.resume()
        } else {
            state.audio.main.pause()
        }
    })

    const targets = Object.values(state.sprites.clothes.wear)
    targets.push(state.sprites.princess)
    state.tweens.princess = this.tweens.add({
        targets: targets,
        x: 320,
        duration: 1300,
        ease: 'Power2',
        paused: true
    });
}

function update () {
    for (let key in state.sprites.clothes.hanger) {
        if (`${state.clothes.hanger.shownGroup}${state.clothes.hanger.currentItem}` === key) {
            state.sprites.clothes.hanger[key].visible = true
        } else {
            state.sprites.clothes.hanger[key].visible = false
        }
    }

    const { dress, top, bottom } = state.clothes.wear
    for (let key in state.sprites.clothes.wear) {
        if (dress === key || top === key || bottom === key) {
            state.sprites.clothes.wear[key].visible = true
        } else {
            state.sprites.clothes.wear[key].visible = false
        }
    }
}

function changeHangerGroup(group) {
    if (state.clothes.hanger.shownGroup == group) return
    state.clothes.hanger.shownGroup = group
    state.clothes.hanger.currentItem = 1
}

function arrowDown() {
    const clothName = `${state.clothes.hanger.shownGroup}${state.clothes.hanger.currentItem - 1}`
    if (clothes[state.clothes.hanger.shownGroup].indexOf(clothName) > -1) {
        state.clothes.hanger.currentItem--
    } else {
        state.clothes.hanger.currentItem = clothes[state.clothes.hanger.shownGroup].length
    }
}

function arrowUp() {
    const clothName = `${state.clothes.hanger.shownGroup}${state.clothes.hanger.currentItem + 1}`
    if (clothes[state.clothes.hanger.shownGroup].indexOf(clothName) > -1) {
        state.clothes.hanger.currentItem++
    } else {
        state.clothes.hanger.currentItem = 1
    }
}

function wearCloth(group, id) {
    if (group === 'dress') {
        state.clothes.wear.top = null
        state.clothes.wear.bottom = null
    }
    if (group === 'top' || group === 'bottom') {
        state.clothes.wear.dress = null
    }
    state.clothes.wear[group] = id
}

function resetClothes() {
    state.clothes.wear.top = null
    state.clothes.wear.bottom = null
    state.clothes.wear.dress = null
}