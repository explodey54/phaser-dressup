'use strict';

const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const walk = require('walk')


const assetsPaths = []
const options = {
    listeners: {
        file: function (root, fileStats, next) {
            assetsPaths.push(path.join(root, fileStats.name))
            next();
        }
    }
}

const walker = walk.walkSync("./assets", options);

module.exports = {
    context: path.join(__dirname, '../app'),
    entry: {
        index: './index.js'
    },
    output: {
        path: path.join(__dirname, '../web'),
        filename: '[name].[hash].js'
    },
    module: {
        loaders: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'index.html',
            inject: true
        }),
        new CopyWebpackPlugin([{from: '../assets', to: 'assets'}]),
        new webpack.DefinePlugin({
            ASSETS_PATHS: JSON.stringify(assetsPaths)
        })
    ]
};
