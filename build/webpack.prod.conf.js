'use strict';

var path = require('path');
var webpack = require('webpack');
var merge = require('webpack-merge');
var baseWebpackConfig = require('./webpack.base.conf.js');

var prodWebpackConfig = {
    entry: {
        index: './index.js'
    }
};

module.exports = merge(baseWebpackConfig, prodWebpackConfig);
